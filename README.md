# Spring Caching tutorial
A demo application to accompany the tutorial on [https://jaspersprengers.nl/2021/06/14/the-subtle-art-of-cache-configuration/](caching with Spring)

 ## Installation:
 `mvn clean install`

 Then start the server as follows:
 `cd target`
 `java -jar caching-demo-1.0-SNAPSHOT.jar`

 This will start a tomcat server on port 8080 and intialize an in-memory H2 database with a number of postal codes for testing (see import.sql)

 Invoke the REST API from your browser (or other client) as follows:

 The temperature API takes a valid Dutch postcode (4 digits, 2 letters)

 `http://localhost:8080/1000AA`


