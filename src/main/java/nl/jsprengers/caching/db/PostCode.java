package nl.jsprengers.caching.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity(name = "postcode")
public class PostCode implements Serializable {
    @Id
    @Column(nullable = false)
    private String code;
    @Column(nullable = false)
    private int stationId;

    public String getCode() {
        return code;
    }

    public int getStationId() {
        return stationId;
    }
}
