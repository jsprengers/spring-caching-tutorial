package nl.jsprengers.caching;

import nl.jsprengers.caching.db.PostCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TemperatureController {

    @Autowired
    CacheablePostcodeService postcodeService;
    @Autowired
    CacheableTemperatureService temperatureService;

    @GetMapping(value = "{postcode}")
    public ResponseEntity<?> getTemperature(@PathVariable(value = "postcode") String postcode) {
        PostCode pcode = postcodeService.getPostcode(postcode);
        if (pcode == null)
            return ResponseEntity.badRequest().body("Unknown postcode: " + postcode);
        float temperatureForCoordinate = temperatureService
                .getTemperatureForCoordinate(pcode.getStationId());
        return new ResponseEntity<Float>(temperatureForCoordinate, HttpStatus.OK);
    }


}
