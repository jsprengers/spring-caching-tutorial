package nl.jsprengers.caching;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

@Service
public class WeatherStation {

    private Map<Integer, RotatingValue> measurements = new HashMap<>();

    @PostConstruct
    public void init() {
        SecureRandom random = new SecureRandom();
        for (int i = 10; i < 100; i++) {
            float change = 5f * random.nextFloat();
            measurements.put(i, new RotatingValue(10f + change, 25f + change, 0.2f));
        }
    }

    public float getTemperatureForWeatherStation(int stationId) {
        return measurements.get(stationId).nextValue();
    }

}
