package nl.jsprengers.caching;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Configuration
public class CacheConfig {

    public static final String POSTCODE_CACHE = "postcode";
    public static final String TEMPERATURE_CACHE = "temperature";

    @Value("${cache.postcode.maximum.size:1000}")
    private int postcodeMaxSize;

    @Value("${cache.expire.temperature.msecs:60000}")
    private int expiryTemperatureMilliSeconds;

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        simpleCacheManager.setCaches(Arrays.asList(
                buildPostCodeCache(),
                buildTemperatureCache()
        ));
        return simpleCacheManager;
    }

    private CaffeineCache buildTemperatureCache() {
        return new CaffeineCache(TEMPERATURE_CACHE, Caffeine
                .newBuilder()
                .expireAfterWrite(expiryTemperatureMilliSeconds, TimeUnit.MILLISECONDS)
                .build(),
                false);
    }

    private CaffeineCache buildPostCodeCache() {
        return new CaffeineCache(POSTCODE_CACHE, Caffeine
                .newBuilder()
                .maximumSize(postcodeMaxSize)
                .build(),
                true);
    }


}
