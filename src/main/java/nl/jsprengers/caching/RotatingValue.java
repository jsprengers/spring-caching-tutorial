package nl.jsprengers.caching;

public class RotatingValue {

    private float min;
    private float max;
    private float increment;

    public RotatingValue(float min, float max, float increment) {
        this.min = min;
        this.max = max;
        this.increment = increment;
        value = min;
    }

    private float value;

    public float nextValue() {
        if (value + increment < min) {
            value = min;
            increment = -increment;
        } else if (value + increment > max) {
            value = max;
            increment = -increment;
        } else {
            value += increment;
        }
        return value;
    }

}
