package nl.jsprengers.caching;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class WeatherStationTest {

    @InjectMocks
    WeatherStation station;

    @Test
    public void testIncrements() {
        station.init();
        float previous = station.getTemperatureForWeatherStation(15);
        for (int i = 0; i < 1000; i++) {
            float forStation = station.getTemperatureForWeatherStation(15);
            assertThat(Math.abs(previous - forStation)).isCloseTo(0.2f, Offset.offset(0.001f));
            previous = forStation;
        }

    }
}
