package nl.jsprengers.caching;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cache.CacheManager;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TemperatureIntegrationTest {

    @LocalServerPort
    int randomServerPort;

    @Autowired
    CacheManager cacheManager;

    TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Test
    public void testGetTemperatureStorePostCode() throws InterruptedException {
        String postcode = "1000AA";
        float temperature = getTemperature(postcode);
        assertThat(temperature).isEqualTo(getTemperature(postcode));
        //let the cached value expire
        Thread.sleep(300);
        assertThat(Math.abs(temperature - getTemperature(postcode))).isCloseTo(0.2f, Offset.offset(0.001f));
    }

    Float getTemperature(String postCode) {
        ResponseEntity<Float> entity =
                testRestTemplate.getForEntity(String.format("http://localhost:%d/%s", randomServerPort, postCode), Float.class);
        if (!entity.getStatusCode().is2xxSuccessful())
            throw new IllegalArgumentException();
        else
            return entity.getBody();
    }

}
