package nl.jsprengers.caching;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RotatingValueTest {

    @Test
    public void testIncrements(){
        RotatingValue rv = new RotatingValue(10, 20, 3);
        assertThat(rv.nextValue()).isEqualTo(13);
        assertThat(rv.nextValue()).isEqualTo(16);
        assertThat(rv.nextValue()).isEqualTo(19);
        assertThat(rv.nextValue()).isEqualTo(20);
        assertThat(rv.nextValue()).isEqualTo(17);
        assertThat(rv.nextValue()).isEqualTo(14);
        assertThat(rv.nextValue()).isEqualTo(11);
        assertThat(rv.nextValue()).isEqualTo(10);
        assertThat(rv.nextValue()).isEqualTo(13);
    }

}
